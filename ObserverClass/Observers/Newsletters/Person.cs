﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClass.Observers.Newsletters
{
    internal class Person : ISubscriber
    {
        public string Name { get; set; }
        public void Update(string message)
        {
            Console.WriteLine(Name + " just got a letter, I wonder who its from: \n" + message);
        }
    }
}
