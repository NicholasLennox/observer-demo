﻿using ObserverClass.Observers.Blogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClass.Observers.Newsletters
{
    internal class NewsletterClient
    {
        public static void Demonstrate()
        {
            Person person = new Person() { Name = "John" };
            Person person2 = new Person() { Name = "Jane" };
            Person person3 = new Person() { Name = "Joan" };
            Person person4 = new Person() { Name = "Jim" };

            NewsLetter thebest = new NewsLetter();
            NewsLetter theworst = new NewsLetter();

            thebest.Subscribe(person);
            thebest.Subscribe(person2);
            thebest.Subscribe(person3);
            thebest.Subscribe(person4);

            theworst.Subscribe(person2);
            theworst.Subscribe(person4);

            thebest.AddNewsletter("There is no competition");
            theworst.AddNewsletter("Why are you here");

            theworst.Unsubscribe(person4);

            theworst.AddNewsletter("I am shookith to the coreith");
        }
    }
}
