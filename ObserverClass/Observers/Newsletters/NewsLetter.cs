﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClass.Observers.Blogs
{
    internal class NewsLetter : IPublisher
    {
        ICollection<ISubscriber> _subscribers = new List<ISubscriber>();
        List<string> _newsletters = new List<string>();

        public void AddNewsletter(string text)
        {
            _newsletters.Add(text);
            // Notify observers
            NotifyObservers();
        } 

        public ICollection<ISubscriber> GetSubscribers()
        {
            return _subscribers;
        }

        public void NotifyObservers()
        {
            // Gets most recent newsletter and calls update
            var newsletter = _newsletters.Last();
            if (newsletter != null)
            {
                foreach(var sub in _subscribers)
                {
                    sub.Update(newsletter);
                }
            }
        }

        public void Subscribe(ISubscriber subscriber)
        {
            _subscribers.Add(subscriber);
        }

        public void Unsubscribe(ISubscriber subscriber)
        {
            _subscribers.Remove(subscriber);
        }
    }
}
