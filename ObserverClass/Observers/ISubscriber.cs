﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClass.Observers
{
    internal interface ISubscriber
    {
        void Update(string message);
    }
}
