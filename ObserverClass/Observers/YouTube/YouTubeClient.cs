﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClass.Observers.YouTube
{
    internal class YouTubeClient
    {
        public static void Demonstrate()
        {
            Channel pewds = new Channel() { Name = "Pewdiepie" };
            Channel tseries = new Channel() { Name = "T-Series" };
            Channel mrbeast = new Channel() { Name = "Mr Beast" };

            tseries.Subscribe(pewds);
            tseries.Subscribe(mrbeast);

            pewds.Subscribe(mrbeast);

            tseries.AddNewVideo("https://www.youtube.com/watch?v=-mMhmE-Syb0");

            pewds.AddNewVideo("https://www.youtube.com/watch?v=6Dh-RL__uN4");

        }
    }
}
