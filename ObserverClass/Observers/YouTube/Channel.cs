﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverClass.Observers.YouTube
{
    internal class Channel : IPublisher, ISubscriber
    {
        public string  Name { get; set; }

        List<string> _videos = new List<string>();
        ICollection<ISubscriber> _subscriber = new List<ISubscriber>();

        public void AddNewVideo(string url)
        {
            _videos.Add(url);
            NotifyObservers();
        }

        public ICollection<ISubscriber> GetSubscribers()
        {
            return _subscriber;
        }

        public void NotifyObservers()
        {
            var latestVideo = _videos.Last();
            if (latestVideo != null)
            {
                foreach(var sub in _subscriber)
                {
                    sub.Update(Name + " just uploaded a new video, check it out: " + latestVideo);
                }
            }
        }

        public void Subscribe(ISubscriber subscriber)
        {
            _subscriber.Add(subscriber);
        }

        public void Unsubscribe(ISubscriber subscriber)
        {
            _subscriber.Remove(subscriber);
        }

        public void Update(string message)
        {
            Console.WriteLine(Name + " just got a notification: \n" + message);
        }
    }
}
