﻿using ObserverClass.Observers.Newsletters;
using ObserverClass.Observers.YouTube;

namespace ObserverClass
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            YouTubeClient.Demonstrate();
        }
    }
}